#pragma once
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <vector>
using namespace std;
//////SORTOWANIE PRZEZ WSTAWIANIE
#pragma region SORT_PRZEZ_WSTAW
void wysw(int a[], int end) {
	for (int i(0); i < end; i++)
		cout << a[i] << "  ";
	cout << endl;
	system("PAUSE");
}

template<typename Typ>
void sort_wstaw(Typ tab[], int end) {
	int j(0);
	Typ pom;
	for (int i(1); i < end; i++) {
		pom = tab[i];
		j = i - 1;
		while (j >= 0 && tab[j] > pom) {
			tab[j + 1] = tab[j];
			j--;
		}
		tab[j + 1] = pom;
	}
}

#pragma endregion
////////////SORTOWANIE PRZEZ KOPCOWANIE
#pragma region KOPCOWANIE

template<typename Typ>//razem z sort_kopc oraz kopcowanie nieuzywane nie zawwsze daja rade dla 1m danych
void wypch_najwiekszy(Typ tab[],int i_it,int end) {//najwieksza wartosc na gore kopca
	int i_bigger=i_it, i_lewe=2*i_it, i_prawe=(2*i_it+1);

	if (i_prawe<end && tab[i_prawe]>tab[i_it]) {		i_bigger = i_prawe; }
	if (i_lewe<end && tab[i_lewe]>tab[i_bigger]) {		i_bigger = i_lewe; }

	if (i_bigger != i_it) {
		swap(tab[i_it], tab[i_bigger]);
		wypch_najwiekszy(tab, i_bigger, end);
	}
}

template<typename Typ>
void kopcowanie(Typ tab[], int end) {

	for (int i((end-1) / 2); i >= 1; i--)
		wypch_najwiekszy<Typ>(tab, i, end); 

	for (int i(end-1); i >= 1; i--) {
		swap(tab[1], tab[i]);
		wypch_najwiekszy<Typ>(tab, 1, i);
	}
}

template<typename Typ>
void sort_kopc(Typ tab[], int end) {
	Typ *pom = new Typ[end + 1];
	//vector<Typ> pom;
	for (int i(1); i < (end + 1); i++) { pom[i] = tab[i - 1]; }
	//for (int i(0); i < (end + 1); i++) { if (i >= 1)pom.push_back(tab[i - 1]); else pom.push_back(0); }
	kopcowanie<Typ>(pom, end);
	for (int j(0); j < end; j++) {
		tab[j] = pom[j + 1];
	}
	delete pom;
}

/*template<typename Typ>
void kopcowanie(Typ tab[], int end) {

	Typ *pom = new Typ[end+1];
	for (int i(0); i < end; i++)
		pom[i + 1] = tab[i];

	for (int i((end) / 2); i >= 1; i--)
		wypch_najwiekszy<Typ>(pom, i, end+1);

	for (int i(end); i >= 1; i--) {
		swap(tab[1], tab[i]);
		wypch_najwiekszy<Typ>(pom, 1, i);
	}

	for (int i(0); i < end; i++)
		tab[i] = pom[i + 1];
}*/

template<typename Typ>
void wypchnij_najwiekszy(Typ tab[], long int i_it, long int end) {
	long int lewa((i_it + 1) * 2 - 1), prawa((i_it + 1) * 2), najw(0);

	if (lewa<end && tab[lewa]>tab[i_it])
		najw = lewa;
	else
		najw = i_it;

	if (prawa<end && tab[prawa]>tab[najw])
		najw = prawa;

	if (najw != i_it) {
		Typ pom = tab[najw];
		tab[najw] = tab[i_it];
		tab[i_it] = pom;
		wypchnij_najwiekszy(tab, najw, end);
	}
}

template<typename Typ>
void kopcowanie2(Typ tab[], long int end) {
	long int rozmiar = end;

	for (long int i((rozmiar - 1) / 2); i >= 0; --i)

		wypchnij_najwiekszy<Typ>(tab, i, rozmiar);
	for (long int j(end - 1); j > 0; --j) {
		//swap(tab[j], tab[0]);
		Typ pom = tab[0];
		tab[0] = tab[j];
		tab[j] = pom;
		--rozmiar;

		wypchnij_najwiekszy<Typ>(tab, 0, rozmiar);
	}
}
#pragma endregion
/////////////////////////////////\
QUICKSORT
#pragma region Quicksort
template<typename Typ>
long int podziel_tab2(Typ tab[], int start, int end) {
	srand(time(NULL));
	long int i_p = (rand() % (end - start)) + start;
	long int l = start, r = end;
	Typ x = tab[i_p];  
	swap(tab[i_p], tab[start]);
	while (true) 
	{
		while (tab[r] > x) r--;
		while (tab[l] < x) l++;
		if (l < r) 
		{
			swap(tab[l], tab[r]);
			l++; r--;
		}
		else { 
			swap(tab[r], tab[start]);
			return r;
		}
	}
}

template<typename Typ>////Dziala zle, bo miesza elementy w while(nie zamienia wiekszych z mniejszymi)
long int podziel_tab(Typ tab[],int start, int end) {
	srand(time(NULL));
	int i_piw= (rand()%(end-start))+start;
	Typ piwot = tab[i_piw];
	swap(tab[end], tab[i_piw]);
	long int l = start; long int r = end - 1;
	do
	 {
		if (tab[l] < piwot) {
			swap(tab[l], tab[r]); }
		if(tab[r]>piwot){ swap(tab[l], tab[r]); }
		l++; r--;
	} while (l <= r);
	swap(tab[end], tab[l]);
	long int poz = l;
	if (r == l) poz = l + 1;
	return poz;
}

template<typename Typ>
void quicksort(Typ tab[], int start, int end) {
	if (start < end) {
		long int i_pivot = podziel_tab2<Typ>(tab,start,end);
		quicksort(tab, start, i_pivot -1);
		quicksort(tab,i_pivot +1 , end);
	}
}
///malejace
template<typename Typ>
long int podziel_tab2_malejace(Typ tab[], int start, int end) {
	srand(time(NULL));
	long int i_p = (rand() % (end - start)) + start;
	long int l = start, r = end;
	Typ x = tab[i_p];
	swap(tab[i_p], tab[start]);
	while (true)
	{
		while (tab[r] < x) r--;
		while (tab[l] > x) l++;
		if (l < r)
		{
			swap(tab[l], tab[r]);
			l++; r--;
		}
		else {
			swap(tab[r], tab[start]);
			return r;
		}
	}
}


template<typename Typ>
void quicksort_malejace(Typ tab[], int start, int end) {
	if (start < end) {
		long int i_pivot = podziel_tab2_malejace<Typ>(tab, start, end);
		quicksort_malejace(tab, start, i_pivot - 1);
		quicksort_malejace(tab, i_pivot + 1, end);
	}
}

#pragma endregion
////////////////////////\
SORTOWANIE INTROSPEKTYWNE
#pragma region INTROSPEKTYWNE
/*template<typename Typ>//razem z sort_introspektywne nie uzywane, nie dawalo rady miedzy innymi przez kopcowanie, ale zostalo juz zmienione
void introspektywne(Typ tab[], int start, int end, double zloz) {
	//cout << "introspeeeee\n";
	long int i_pivot;

	while (end - start > 0) {
		if (zloz == 0)
			
			sort_kopc<Typ>(&tab[start], end - start + 1);
		else {
			i_pivot = podziel_tab2<Typ>(tab, start, end);
			introspektywne(tab, i_pivot + 1, end, zloz - 1);
			end = i_pivot - 1;
			}
	}
}

template<typename Typ>
void sort_introspektywne(Typ tab[], int end) {
	introspektywne<Typ>(tab, 0, end - 1, int(2 * log(double(end))));
	sort_wstaw<Typ>(tab, end);
}*/


template<typename Typ>
void sort_introspekt(Typ tab[], long int end) {
	long int i_pivot = podziel_tab2<Typ>(tab, 0, end - 1);
	if (i_pivot < 16) {
		sort_wstaw<Typ>(tab, end);
	}
	else if (i_pivot > 2 * log(end)) {
		kopcowanie2<Typ>(tab, end);
	}
	else {
		quicksort<Typ>(tab, 0, end - 1);
	}
}
#pragma endregion
///////////////////////////////////////\
SORTOWANIE PRZEZ SCALANIE
#pragma region SORT_SCAL

/*template<typename Typ>
void scal(Typ tab[], int start,int middle, int end){
	Typ *tab_pom = new Typ[end];
	int j(start), k(middle + 1);//ustawia dwa znaczniki na poczatek i srodek

	for (int i(start); i <= end; i++) {		tab_pom[i] = tab[i];	}//przepisuje scalana czesc do tablicy pomocniczej
	
	for (int l(start); l <= end; l++) {
		if (k <= end && j < (middle+1)) {
			if (tab_pom[j] < tab_pom[k])
				tab[l] = tab_pom[j++];
			else
				tab[l] = tab_pom[k++];		
		}
		else { 
			if(j<(middle+1))
				tab[l] = tab_pom[j++];
		}
	}
}

template<typename Typ>
void sort_scal(Typ tab[], int start, int end){
	if (end > start) {
		int middle = (start + end) / 2;
		sort_scal(tab, start, middle);
		sort_scal(tab, middle+1, end);
		scal<Typ>(tab, start, middle, end);
	}
	else return;
}*/

/*template<typename Typ>
void scal(Typ tab[], long int start, long int middle, long int end) {
	//Typ *tab_pom = new Typ[end+1];
	vector<Typ> v_pom;
	long int j(start), k(middle + 1);//ustawia dwa znaczniki na poczatek i srodek

	//for (int i(start); i <= end; i++) { tab_pom[i] = tab[i]; }//przepisuje scalana czesc do tablicy pomocniczej
	for (int i(0); i <= end; i++) { if (i >= start)v_pom.push_back(tab[i]); else v_pom.push_back(0); }//przepisuje scalana czesc do tablicy pomocniczej

	for (int l(start); l <= end; l++) {
		if (k <= end && j < (middle + 1)) {
			if (v_pom[j] < v_pom[k])
				tab[l] = v_pom[j++];
			else
				tab[l] = v_pom[k++];
		}
		else {
			if (j<(middle + 1))
				tab[l] = v_pom[j++];
		}
	}
}

template<typename Typ>
void sort_scal(Typ tab[], long int start,long int end) {
	if (end > start) {
		long int middle = (start + end) / 2;
		sort_scal(tab, start, middle);
		sort_scal(tab, middle + 1, end);
		scal<Typ>(tab, start, middle, end);
	}
	else return;
}*/


template<typename Typ>
void sort_scal(Typ tab[], long int start, long int end) {
	if (end > start) {
		long int middle = (start + end) / 2;
		sort_scal(tab, start, middle);
		sort_scal(tab, middle + 1, end);

		long int ile(end - start + 1);
		Typ* pom = new Typ[ile];
		long int i(start), j(middle + 1);//ustawia dwa znaczniki na poczatek i srodek
		for (int k(0); k<ile; k++) {
			if (j>end || (i <= middle && tab[i]<tab[j])) {
				pom[k] = tab[i];
				i++;
			}
			else {
				pom[k] = tab[j];
				j++;
			}
		}
		for (int k(0), i = start; k < ile; k++, i++) {
			tab[i] = pom[k];
		}
		delete pom;
	}
	else return;
}


/////malejace
template<typename Typ>
void sort_scal_malej(Typ tab[], int start, int end) {
	if (end > start) {
		long int middle = (start + end) / 2;
		sort_scal(tab, start, middle);
		sort_scal(tab, middle + 1, end);

		long int ile(end - start + 1);
		Typ* pom = new Typ[ile];
		long int i(start), j(middle + 1);//ustawia dwa znaczniki na poczatek i srodek
		for (int k(ile-1); k>=0; k--) {
			if (j>end || (i <= middle && tab[i]<tab[j])) {
				pom[k] = tab[i];
				i++;
			}
			else {
				pom[k] = tab[j];
				j++;
			}
		}
		for (int k(0), i = start; k < ile; k++, i++) {
			tab[i] = pom[k];
		}
		delete pom;
	}
	else return;
}
#pragma endregion
////////////////////////



