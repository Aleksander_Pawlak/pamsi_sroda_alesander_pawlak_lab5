#pragma once
#include <Windows.h>
#include <iostream>
#include <ctime>
#include <cstdlib>
#include "sort_func.h"
#include <fstream>

using namespace std;
void sor(int ktore) {
	int rozmiar(0);
	cout << "Podaj rozmiar tablicy:\n";
	cin >> rozmiar;
	srand(time(NULL));
	int *tab = new int[rozmiar];
	for (int i(0); i < rozmiar; i++)	tab[i] = rand() % 1000;
	cout << "Tablica przed sortowaniem:\n";
	for (int k(0); k < rozmiar; k++) {
		if (k != 0 && k % 10 == 0) cout << endl;
		cout << tab[k] << "  ";
	}
	cout << endl;
	switch (ktore) {
	case 1: sort_scal<int>(tab, 0, rozmiar - 1); break;
	case 2: quicksort<int>(tab, 0, rozmiar - 1); break;
	case 3: sort_introspekt<int>(tab, rozmiar); break;
	}
	cout << "Posortowana tablica:\n";
	for (int j(0); j < rozmiar; j++) {
		if (j != 0 && j % 10 == 0) cout << endl;
		cout << tab[j] << "  ";
	}
	cout << endl;
}

void pokaz() {
	int wyb(0);
	do {
		cout << "wybierz sposob sortowania:\n";
		cout << "1. Przez scalanie\n";
		cout << "2. Quicksort\n";
		cout << "3. introspektywne\n";
		cout << "0. cofnij\n";
		cin >> wyb;
		switch (wyb) {
		case 1: sor(1); break;
		case 2: sor(2); break;
		case 3: sor(3); break;

		deafult: cout << "Niepoprawny wybor\n"; break;
		}
	} while (wyb != 0);

}



void menu() {
	srand(time(NULL));
	LARGE_INTEGER frequency;        // ticks per second
	LARGE_INTEGER t1, t2;           // ticks
	double elapsedTime;
	QueryPerformanceFrequency(&frequency);

	QueryPerformanceCounter(&t1);//start zegara

	QueryPerformanceCounter(&t2);//stop zegara
	elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
	cout << "Czas wykonania:  " << elapsedTime << " ms.\n";

	int wybor(0);
	int wybor2(0);
	do {
		cout << "sprawdzanie roznych rodzajow sortowania:\n\n";
		cout << "1. Proba sortowania przez scalanie\n";
		cout << "2. Proba sortowania typu quicksort\n";
		cout << "3. Proba sortowania introspektywnego\n";
		cout << "4. odswiez ekran\n";
		cout << "0. wyjscie z programu\n";
		cin >> wybor; cout << endl;
		switch (wybor) {
			case 1: {
				do {
					cout << "Wybierz typ zmiennych\n";
					cout << "1. int\n";
					cout << "2. float\n";
					cout << "3. string\n";
					cout << "4. odswiez ekran\n";
					cout << "0. cofnij\n";
					cin >> wybor2;
					switch (wybor2) {
					case 1: break;



					}
				} while (wybor2 != 0);
				break; }
			case 2: {break; }
			case 3: {break; }
			case 4: system("cls");
		default: cout << "Niepoprawna opcja, sproboj jeszcze raz.\n";
		}
	} while (wybor != 0);
}

template<typename Typ>
void testy(){
	int wielk, ile_posort,czy_odwr,ktore(0),zakres;
	Typ* tab_test[100];
	Typ* tab;
	srand(time(NULL));
	srand(time(NULL));
	LARGE_INTEGER frequency;        // ticks per second
	LARGE_INTEGER t1[100], t2[100];           // ticks
	double elapsedTime[100];
	QueryPerformanceFrequency(&frequency);

	cout << "Podaj wielkosc tablicy:   \n";
	cin >> wielk;

	cout << "Podaj gorny zakres liczb w tablicy\n";
	cin >> zakres;

	cout << "\nPodaj ilosc danych juz posortowanych:    ";
	cin >> ile_posort;

	cout << "\nCzy maja byc wczesniej w odwrotnej kolejnosci?(1=tak)\n";
	cin >> czy_odwr;

	cout << "Wybierz typ sortowania:\n";
	cout << "1. Przez scalanie\n";
	cout << "2. Quicksort\n";
	cout << "3. Introspektywne\n";
	cin >> ktore;

	for (int k(0); k < 100; k++)
		tab_test[k] = new Typ[wielk];
	//tab = new Typ[wielk];
	
	for (int c(0); c < 100; c++) {
		for (int i(0); i < wielk; i++)
			tab_test[c][i] = rand() % (zakres);
	}
	
	for(int t(0);t<100;t++)
		quicksort<Typ>(tab_test[t], 0, ile_posort - 1);

	if (czy_odwr == 1) {
	for(int p(0);p<100;p++)
		quicksort_malejace<Typ>(tab_test[p], 0, wielk - 1);
	}
	if (wielk >= 100) {
		cout << "Nieposortowany koniec tablicy:\n";
		for (int i(wielk - 100); i < wielk; i++) {
			if (i != 0 && i % 15 == 0) cout << endl;
			cout << tab_test[0][i] << "  ";
		}
		cout << endl;
	}

		switch (ktore) {
		case 1: {
			for (int j(0); j < 100; j++) {
				QueryPerformanceCounter(&t1[j]);//start zegara
				sort_scal<Typ>(tab_test[j], 0, wielk - 1);
				QueryPerformanceCounter(&t2[j]);//stop zegara
			}
			break;
		}
		case 2: {
			for (int j(0); j < 100; j++) {
				QueryPerformanceCounter(&t1[j]);//start zegara
				quicksort<Typ>(tab_test[j], 0, wielk - 1);
				QueryPerformanceCounter(&t2[j]);//stop zegara
			}
			break;
		}
		case 3: {
			for (int j(0); j < 100; j++) {
				QueryPerformanceCounter(&t1[j]);//start zegara
				//sort_introspektywne<Typ>(tab_test[j], wielk - 1);
				sort_introspekt<Typ>(tab_test[j], wielk);
				QueryPerformanceCounter(&t2[j]);//stop zegara
				//cout << "sortuje po raz " << j+1 << endl;
			}
			break;
		}
		default: cout << "Niestety sortowanie bylo zle wybrane\n";
		}
		cout << "Bylum tu\n";
		for(int l(0);l<100;l++)
			elapsedTime[l] = (t2[l].QuadPart - t1[l].QuadPart) * 1000.0 / frequency.QuadPart;

		//ofstream plik("testy.txt");
		fstream plik("testy.txt", std::ios::app | std::ios::ate);
		for (int q(0); q < 100; q++)
			plik << elapsedTime[q] << ", ";
		plik << endl;
		plik.close();
		if (wielk >= 100) {
			cout << "Posortowany koniec tablicy:\n";
			for (int i(wielk - 100); i < wielk; i++) {
				if (i != 0 && i % 15 == 0) cout << endl;
				cout << tab_test[0][i] << "  ";
			}
			cout << endl;
		}
}